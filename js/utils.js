//Detect collision between 
function rectangularCollision({
    rectangle1,
    rectangle2
}) {
    return (
        rectangle1.attackBox.position.x + rectangle1.attackBox.width >= rectangle2.position.x && rectangle1.attackBox.position.x <= rectangle2.position.x + rectangle2.width && rectangle1.attackBox.position.y + rectangle1.attackBox.height >= rectangle2.position.y && rectangle1.attackBox.position.y <= rectangle2.position.y + rectangle2.height
    )
}

//Determine the winner
function determineWinner({
    player,
    ennemy,
    timerId
}) {
    
    clearTimeout(timerId); //annule le timeout correspondant à l'ID passé en para
    
    document.querySelector('#result').style.display = 'flex';
    
    if (player.health === enemy.health) {
        document.querySelector('#result').innerHTML = 'Tie';
    } else if (player.health > enemy.health) {
        document.querySelector('#result').innerHTML = 'Player1 wins';
    } else if (player.health < enemy.health) {
        document.querySelector('#result').innerHTML = 'Enemy wins';
    }
}

//Decrease timer over time
let timer = 60;
let timerId;

function decreaseTimer() {
    if (timer > 0) {
        timerId = setTimeout(decreaseTimer, 1000) //Lorsque le timeout est cleared, il ne relancera pas l'appel de la fonction
        timer--;
        document.querySelector('#timer').innerHTML = timer;
    }

    if (timer === 0) {
        determineWinner({player, enemy});
    }
}