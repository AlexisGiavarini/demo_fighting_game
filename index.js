const canvas = document.querySelector('canvas');
const c = canvas.getContext('2d');

canvas.width = 1024;
canvas.height = 720;

c.fillRect(0, 0, canvas.width, canvas.height);

const gravity = 0.7;

const background = new Sprite({
    position: {
        x: 0,
        y: 0
    },
    imageSrc: './img/Background.png',
    width: canvas.width,
    height: canvas.height,
    scaleWidth: 1.1034,
    scaleHeight: 0.908
})

const obelisk = new Sprite({
    position: {
        x: 480,
        y: 380
    },
    imageSrc: './img/FlyingObelisk.png',
    width: canvas.width * 1.75,
    height: 300,
    frameMax: 13,
    framesHold: 10,
    scaleWidth: 0.75,
    scaleHeight: 0.75
})

//Player
const player = new Fighter({
    position: {
        x: 107,
        y: 100
    },
    velocity: {
        x: 0,
        y: 10
    },
    offset: {
        x: 0,
        y: 0
    },
    imageSrc: './img/samuraiMack/Idle.png',
    frameMax: 8,
    framesHold: 5,
    scaleWidth: 2.3,
    scaleHeight: 2.3,
    offset: {
        x: 215,
        y: 140
    },
    sprites: {
        idle: {
            imageSrc: './img/samuraiMack/Idle.png',
            frameMax: 8
        },
        run: {
            imageSrc: './img/samuraiMack/Run.png',
            frameMax: 8,
            image: new Image()
        },
        jump: {
            imageSrc: './img/samuraiMack/Jump.png',
            frameMax: 2,
            image: new Image()
        },
        fall: {
            imageSrc: './img/samuraiMack/Fall.png',
            frameMax: 2,
            image: new Image()
        },
        attack1: {
            imageSrc: './img/samuraiMack/Attack1.png',
            frameMax: 6,
            image: new Image(),
            adjustingFramesHold: 3 //this attribute is used to adjust attack speed frames
        },
        takeHit: {
            imageSrc: './img/samuraiMack/TakeHit-white-silhouette.png',
            frameMax: 4
        },
        death: {
            imageSrc: './img/samuraiMack/Death.png',
            frameMax: 6
        }
    },
    attackBox: {
        offset: {
            x: 100,
            y: 50
        },
        width: 120,
        height: 50
    }
})

//Enemy
const enemy = new Fighter({
    position: {
        x: 867,
        y: 100
    },
    velocity: {
        x: 0,
        y: 10
    },
    color: 'blue',
    offset: {
        x: -50,
        y: 0
    },
    imageSrc: './img/kenji/Idle.png',
    frameMax: 4,
    framesHold: 5,
    scaleWidth: 2.3,
    scaleHeight: 2.3,
    offset: {
        x: 215,
        y: 152
    },
    sprites: {
        idle: {
            imageSrc: './img/kenji/Idle.png',
            frameMax: 4
        },
        run: {
            imageSrc: './img/kenji/Run.png',
            frameMax: 8,
            image: new Image()
        },
        jump: {
            imageSrc: './img/kenji/Jump.png',
            frameMax: 2,
            image: new Image()
        },
        fall: {
            imageSrc: './img/kenji/Fall.png',
            frameMax: 2,
            image: new Image()
        },
        attack1: {
            imageSrc: './img/kenji/Attack1.png',
            frameMax: 4,
            image: new Image(),
            adjustingFramesHold: 5
        },
        takeHit: {
            imageSrc: './img/kenji/TakeHit.png',
            frameMax: 3
        },
        death: {
            imageSrc: './img/kenji/Death.png',
            frameMax: 7
        }
    },
    attackBox: {
        offset: {
            x: -170,
            y: 50
        },
        width: 120,
        height: 50
    }
})

//prevent loss of movement when one key gets up while another one is down
const keys = {
    d: {
        pressed: false
    },
    q: {
        pressed: false
    },
    z: {
        pressed: false
    },
    ArrowRight: {
        pressed: false
    },
    ArrowLeft: {
        pressed: false
    },
    ArrowUp: {
        pressed: false
    },
}

decreaseTimer();

//Alter sprite position
function animate() {
    window.requestAnimationFrame(animate) // function to loop over each frame
    c.fillStyle = 'black';
    c.fillRect(0, 0, canvas.width, canvas.height)

    background.update();
    obelisk.update();
    
    c.fillStyle = 'rgba(255, 255, 255, 0.1)'
    c.fillRect(0,0, canvas.width, canvas.height)

    player.update();
    enemy.update();

    player.velocity.x = 0;
    enemy.velocity.x = 0;

    //for each loop of animation we check if one key is pressed down
    //player
    if (keys.d.pressed && player.lastKey === 'd') {
        player.velocity.x = 5;
        player.switchSprite('run');
    } else if (keys.q.pressed && player.lastKey === 'q') {
        player.velocity.x = -5;
        player.switchSprite('run');
    } else {
        player.switchSprite('idle');
    }

    if (player.velocity.y < 0) {
        player.switchSprite('jump');
    } else if (player.velocity.y > 0) {
        player.switchSprite('fall');
    }


    //enemy
    if (keys.ArrowLeft.pressed && enemy.lastKey === 'ArrowLeft') {
        enemy.velocity.x = -5;
        enemy.switchSprite('run');
    } else if (keys.ArrowRight.pressed && enemy.lastKey === 'ArrowRight') {
        enemy.velocity.x = 5;
        enemy.switchSprite('run');
    } else {
        enemy.switchSprite('idle');
    }

    if (enemy.velocity.y < 0) {
        enemy.switchSprite('jump');
    } else if (enemy.velocity.y > 0) {
        enemy.switchSprite('fall');
    }

    //Attack collision
    if (rectangularCollision({
            rectangle1: player,
            rectangle2: enemy
        }) && player.isAttacking && player.currentFrame === 4) {
        enemy.takeHit();
        player.isAttacking = false;
        gsap.to('#enemy-life', {
            width: enemy.health + '%'
        })
    }

    if (rectangularCollision({
            rectangle1: enemy,
            rectangle2: player
        }) && enemy.isAttacking && enemy.currentFrame === 2) {
        player.takeHit();
        enemy.isAttacking = false;
        gsap.to('#player-life', {
            width: player.health + '%'
        })
    }

    //Game over based on health
    if (player.health <= 0 || enemy.health <= 0) {
        determineWinner({
            player,
            enemy,
            timerId
        });
    }
}

animate()

window.addEventListener('keydown', (event) => {
    //player
    if (!player.dead) {
        switch (event.key) {
            case 'd':
                keys.d.pressed = true;
                player.lastKey = 'd';
                break;

            case 'q':
                keys.q.pressed = true;
                player.lastKey = 'q';
                break;

            case 'z':
                //doubleJump
                if (!keys.z.pressed && player.doubleJump < 2) {
                    player.velocity.y = -17;
                    player.doubleJump += 1;
                }
                keys.z.pressed = true;
                break;

            case ' ':
                //attack
                if (player.attackKey.pressed == false && player.isAttacking == false) {
                    player.attackKey.pressed = true;
                    player.attack();
                }
                break;
        }
    }

    //enemy
    if (!enemy.dead) {
        switch (event.key) {
            case 'ArrowRight':
                keys.ArrowRight.pressed = true;
                enemy.lastKey = "ArrowRight";
                break;

            case 'ArrowLeft':
                keys.ArrowLeft.pressed = true;
                enemy.lastKey = "ArrowLeft";
                break;

            case 'ArrowUp':
                //doubleJump
                if (!keys.ArrowUp.pressed && enemy.doubleJump < 2) {
                    enemy.velocity.y = -17;
                    enemy.doubleJump += 1;
                }
                keys.ArrowUp.pressed = true;
                break;

            case 'Enter':
                if (enemy.attackKey.pressed == false && enemy.isAttacking == false) {
                    enemy.attackKey.pressed = true;
                    enemy.attack();
                }
                break;
        }
    }
})

window.addEventListener('keyup', (event) => {
    switch (event.key) {
        //player
        case 'd':
            keys.d.pressed = false;
            if (keys.q.pressed) { // Allow to change the char. direction easily
                player.lastKey = 'q';
            }
            break;

        case 'q':
            keys.q.pressed = false;

            if (keys.d.pressed) {
                player.lastKey = 'd';
            }
            break;
        case 'z':
            keys.z.pressed = false;
            break;

            //enemy
        case 'ArrowRight':
            keys.ArrowRight.pressed = false;
            if (keys.ArrowLeft.pressed) {
                enemy.lastKey = 'ArrowLeft';
            }
            break;

        case 'ArrowLeft':
            keys.ArrowLeft.pressed = false;

            if (keys.ArrowRight.pressed) {
                enemy.lastKey = 'ArrowRight';
            }
            break;

        case 'ArrowUp':
            keys.ArrowUp.pressed = false;
            break;
    }
})
