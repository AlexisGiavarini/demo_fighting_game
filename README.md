This very, very basic 2D fight style game is based on Chris courses video. I just changed a few things to improve it a little but it has to be considered just as a 'test' project.

Controls:
Q and D to move player 1
Z player 1 jump
Space player 1 attack

left arrow and right arrow  to move player 2
up arrow  player 2 jump
Enter player 2 attack
