//**************************************//
//*************** Sprite ***************//
//**************************************//
class Sprite {
    constructor({
        position,
        imageSrc,
        width,
        height,
        frameMax = 1,
        framesHold,
        scaleWidth = 1,
        scaleHeight = 1,
        offset = { //Used to selelect the part of the image we want
            x: 0,
            y: 0
        }
    }) {
        this.position = position;
        this.width = width;
        this.height = height;
        this.image = new Image();
        this.image.src = imageSrc;
        this.frameMax = frameMax;
        this.framesHold = framesHold;
        this.currentFrame = 0;
        this.framesElapsed = 0;
        this.scaleWidth = scaleWidth;
        this.scaleHeight = scaleHeight;
        this.offset = offset;
    }

    draw() {

        c.drawImage(
            this.image,
            this.currentFrame * (this.image.width / this.frameMax),
            0,
            this.image.width / this.frameMax,
            this.image.height,
            this.position.x - this.offset.x, //Move the start of the selection of the image where we need it
            this.position.y - this.offset.y,
            (this.image.width / this.frameMax) * this.scaleWidth,
            (this.image.height) * this.scaleHeight
        );
    }

    animateFrames() {
        this.framesElapsed++
        if (this.framesElapsed % this.framesHold === 0) {
            if (this.currentFrame < this.frameMax - 1) {
                this.currentFrame++
            } else {
                this.currentFrame = 0
            }
        }
    }

    update() {
        this.draw();
        this.animateFrames();
    }
}


//***************************************//
//*************** Fighter ***************//
//***************************************//
class Fighter extends Sprite {
    constructor({
        position,
        velocity,
        color = 'red',
        imageSrc,
        frameMax = 1,
        framesHold,
        scaleWidth = 1,
        scaleHeight = 1,
        offset = {
            x: 0,
            y: 0
        },
        sprites,
        attackBox = {
            offset: {},
            width: undefined,
            height: undefined,
        }
    }) {
        super({
            position,
            imageSrc,
            frameMax,
            framesHold,
            scaleWidth,
            scaleHeight,
            offset
        })
        this.velocity = velocity;
        this.width = 50;
        this.height = 150;
        this.lastKey;
        this.doubleJump;
        this.attackBox = {
            position: {
                x: this.position.x,
                y: this.position.y
            },
            offset: attackBox.offset,
            width: attackBox.width,
            height: attackBox.height,
        }
        this.isAttacking = false;
        this.attackKey = {
            pressed: false
        }
        this.color = color;
        this.health = 100;
        this.currentFrame = 0;
        this.framesElapsed = 0;
        this.sprites = sprites;
        this.dead = false;

        //Switch between sprites depending on the character's movement
        //Sprite is choosing when a key is pressed
        for (const sprite in this.sprites) {
            sprites[sprite].image = new Image()
            sprites[sprite].image.src = sprites[sprite].imageSrc
        }
    }

    update() {
        this.draw();
        if(!this.dead) {
            this.animateFrames();
        }
        

        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;

        this.attackBox.position.x = this.position.x + this.attackBox.offset.x;
        this.attackBox.position.y = this.position.y + this.attackBox.offset.y;

        //attackBoc
        //c.fillRect(this.attackBox.position.x, //this.attackBox.position.y,this.attackBox.width, this.attackBox.height)

        //Deal with the fall and the bottom of the screen
        if (this.position.y + this.height + this.velocity.y >= canvas.height - 45) {
            this.velocity.y = 0;
            this.doubleJump = 0;
            this.position.y = 525;
        } else {
            this.velocity.y += gravity;
        }

        //prevent the player from going outside the screen
        if (this.position.x + this.velocity.x <= 0) {
            this.position.x = 0;
        } else if (this.position.x + this.width + this.velocity.x >= canvas.width) {
            this.position.x = canvas.width - this.width;
        }
    }

    attack() {
        let tmpFramesHold = this.framesHold;
        this.framesHold = this.sprites.attack1.adjustingFramesHold; //Attack quicker
        this.switchSprite('attack1');
        this.isAttacking = true;
        //Period of time when a player is attacking
        setTimeout(() => {
            this.isAttacking = false;
            this.attackKey.pressed = false;
            this.framesHold = tmpFramesHold;
        }, 300)
    }

    takeHit() {

        this.health -= 20;
        if (this.health <= 0) {
            this.switchSprite('death');
        }else{
            this.switchSprite('takeHit');
        }
    }

    switchSprite(sprite) {
        //No more animation if we are dead
        if (this.image === this.sprites.death.image){
            if(this.currentFrame === this.sprites.death.frameMax -1){
                this.dead = true;
            }
            return
        } 
        
        //Overrides all other animations with attack
        if (this.image === this.sprites.attack1.image && this.currentFrame < this.sprites.attack1.frameMax - 1) return

        //Same with takeHite
        if (this.image === this.sprites.takeHit.image && this.currentFrame < this.sprites.takeHit.frameMax - 1) return

        switch (sprite) {
            case 'idle':
                if (this.image !== this.sprites.idle.image) {
                    this.image = this.sprites.idle.image;
                    this.frameMax = this.sprites.idle.frameMax;
                    this.currentFrame = 0; // reset the browsing of the frames
                }

                break
            case 'run':
                if (this.image !== this.sprites.run.image) {
                    this.image = this.sprites.run.image;
                    this.frameMax = this.sprites.run.frameMax;
                    this.currentFrame = 0; // reset the browsing of the frames
                }
                break
            case 'jump':
                if (this.image !== this.sprites.jump.image) {
                    this.image = this.sprites.jump.image;
                    this.frameMax = this.sprites.jump.frameMax;
                    this.currentFrame = 0; // reset the browsing of the frames
                }
                break
            case 'fall':
                if (this.image !== this.sprites.fall.image) {
                    this.image = this.sprites.fall.image;
                    this.frameMax = this.sprites.fall.frameMax;
                    this.currentFrame = 0;
                }
                break
            case 'attack1':
                if (this.image !== this.sprites.attack1.image) {
                    this.image = this.sprites.attack1.image;
                    this.frameMax = this.sprites.attack1.frameMax;
                    this.currentFrame = 0;
                }
                break

            case 'takeHit':
                if (this.image !== this.sprites.takeHit.image) {
                    this.image = this.sprites.takeHit.image;
                    this.frameMax = this.sprites.takeHit.frameMax;
                    this.currentFrame = 0;
                }
                break

            case 'death':
                if (this.image !== this.sprites.death.image) {
                    this.image = this.sprites.death.image;
                    this.frameMax = this.sprites.death.frameMax;
                    this.currentFrame = 0;
                }
                break
        }

    }
}
